/*
 * Copyright (c) 2013. Gerrit Grunwald
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package eu.hansolo.fx.validation;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFieldBuilder;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleButtonBuilder;
import javafx.scene.layout.HBox;
import javafx.scene.layout.HBoxBuilder;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.StackPaneBuilder;
import javafx.scene.layout.VBox;
import javafx.scene.layout.VBoxBuilder;
import javafx.scene.paint.Color;
import javafx.stage.Stage;


/**
 * Created by
 * User: hansolo
 * Date: 09.04.13
 * Time: 19:15
 */
public class Demo extends Application {
    private ValidationPane            validationPane;
    private TextField                 field1;
    private TextField                 field2;
    private TextField                 field3;
    private TextField                 field4;
    private ToggleButton              button1;
    private ToggleButton              button2;
    private ToggleButton              button3;
    private ToggleButton              button4;
    private EventHandler<ActionEvent> handler;


    @Override public void init() {
        handler = actionEvent -> {
            ToggleButton SRC = (ToggleButton) actionEvent.getSource();
            if (SRC.equals(button1)) {
                if (button1.isSelected()) {
                    button1.setText("valid");
                    validationPane.setState(field1, Validator.State.VALID);
                } else {
                    button1.setText("invalid");
                    validationPane.setState(field1, Validator.State.INVALID);
                }
            } else if (SRC.equals(button2)) {
                if (button2.isSelected()) {
                    button2.setText("valid");
                    validationPane.setState(field2, Validator.State.VALID);
                } else {
                    button2.setText("invalid");
                    validationPane.setState(field2, Validator.State.INVALID);
                }
            } else if (SRC.equals(button3)) {
                if (button3.isSelected()) {
                    button3.setText("info");
                    validationPane.setState(field3, Validator.State.INFO);
                } else {
                    button3.setText("none");
                    validationPane.setState(field3, Validator.State.CLEAR);
                }
            } else if (SRC.equals(button4)) {
                if (button4.isSelected()) {
                    button4.setText("optional");
                    validationPane.setState(field4, Validator.State.OPTIONAL);
                } else {
                    button4.setText("none");
                    validationPane.setState(field4, Validator.State.CLEAR);
                }
            }
        };

        field1  = TextFieldBuilder.create().promptText("text1").build();
        field2  = TextFieldBuilder.create().promptText("text2").build();
        field3  = TextFieldBuilder.create().promptText("text3").build();
        field4  = TextFieldBuilder.create().promptText("text4").build();

        button1 = ToggleButtonBuilder.create().text("invalid").prefWidth(100).onAction(handler).build();
        button2 = ToggleButtonBuilder.create().text("invalid").prefWidth(100).onAction(handler).build();
        button3 = ToggleButtonBuilder.create().text("none").prefWidth(100).onAction(handler).build();
        button4 = ToggleButtonBuilder.create().text("none").prefWidth(100).onAction(handler).build();

        validationPane = new ValidationPane();
        validationPane.addAll(field1, field2, field3, field4);

        // React on Validation events fired by the ValidationPane
        validationPane.setOnValid(validationEvent -> {
            System.out.println(validationEvent.getNode() + " is now valid");
        });
        validationPane.setOnInvalid(validationEvent -> {
            System.out.println(validationEvent.getNode() + " is now invalid");
        });
        validationPane.setOnInfo(validationEvent -> {
            System.out.println(validationEvent.getNode() + " is now info");
        });
        validationPane.setOnOptional(validationEvent -> {
            System.out.println(validationEvent.getNode() + " is now optional");
        });
        validationPane.setOnClear(validationEvent -> {
            System.out.println(validationEvent.getNode() + " is now cleared");
        });
    }

    @Override public void start(Stage stage) {
        HBox row1 = HBoxBuilder.create()
                               .spacing(10)
                               .children(new Label("Label 1"), field1, button1)
                               .build();
        HBox row2 = HBoxBuilder.create()
                               .spacing(10)
                               .children(new Label("Label 2"), field2, button2)
                               .build();
        HBox row3 = HBoxBuilder.create()
                               .spacing(10)
                               .children(new Label("Label 3"), field3, button3)
                               .build();
        HBox row4 = HBoxBuilder.create()
                               .spacing(10)
                               .children(new Label("Label 4"), field4, button4)
                               .build();

        VBox col = VBoxBuilder.create()
                              .spacing(20)
                              .padding(new Insets(10, 10, 10, 10))
                              .children(row1, row2, row3, row4)
                              .build();

        StackPane pane = StackPaneBuilder.create()
                                         .children(col, validationPane)
                                         .build();

        Scene scene = new Scene(pane, Color.DARKGRAY);

        stage.setTitle("Validation overlay");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
